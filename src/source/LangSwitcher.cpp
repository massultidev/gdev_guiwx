/* Copyright by: P.J. Grochowski */

#include "gdev/guiWX/LangSwitcher.h"

#include <wx/arrstr.h>

namespace gdev {
namespace guiWx {

LangSwitcher::Ptr LangSwitcher::mInstance = NULL;

LangSwitcher::LangSwitcher()
	: mSPtr(gdev::utility::Strings::getInstance())
{}
LangSwitcher::~LangSwitcher() {
}
LangSwitcher::Ptr LangSwitcher::getInstance() {
	if(mInstance.isNull()) {
		mInstance = new LangSwitcher();
	}
	return mInstance;
}

void LangSwitcher::setLang(const std::string& lang) {
	Poco::ScopedLock < Poco::Mutex > lock( mMtx );

	mSPtr->setLang( lang );

	{
		typedef std::vector < std::pair < wxStaticBox*, std::string > >::iterator It;
		for( It it = mSBoxs.begin(), itEnd = mSBoxs.end(); it != itEnd; it++ ) {
			it->first->SetLabel( _getStrWX( it->second ) );
		}
	}
	{
		typedef std::vector < std::pair < wxStaticText*, std::string > >::iterator It;
		for( It it = mSTxts.begin(), itEnd = mSTxts.end(); it != itEnd; it++ ) {
			it->first->SetLabel( _getStrWX( it->second ) );
		}
	}
	{
		typedef std::vector < std::pair < wxCheckBox*, std::string > >::iterator It;
		for( It it = mChecks.begin(), itEnd = mChecks.end(); it != itEnd; it++ ) {
			it->first->SetLabel( _getStrWX( it->second ) );
		}
	}
	{
		typedef std::vector < std::pair < wxButton*, std::string > >::iterator It;
		for( It it = mBtns.begin(), itEnd = mBtns.end(); it != itEnd; it++ ) {
			it->first->SetLabel( _getStrWX( it->second ) );
		}
	}
	{
		typedef std::vector < std::pair < wxChoice*, std::vector < std::string > > >::iterator It;
		for( It it = mCboxs.begin(), itEnd = mCboxs.end(); it != itEnd; it++ )
		{
			wxArrayString items;
			for( std::size_t i=0; i<it->second.size(); i++ ) {
				items.Add( _getStrWX( it->second.at( i ) ) );
			}
			it->first->Set( items );
			it->first->SetSelection( 0 );
		}
	}
	{
		typedef std::vector < Poco::Tuple < wxMenuBar*, std::vector < std::pair < std::size_t, std::string > >, std::vector < std::pair < std::size_t, std::string > > > >::iterator It;
		typedef std::vector < std::pair < std::size_t, std::string > >::iterator It2;
		for( It it = mMBars.begin(), itEnd = mMBars.end(); it != itEnd; it++ )
		{
			wxMenuBar* menuBar = it->get<0>();
			std::vector < std::pair < std::size_t, std::string > > menus = it->get<1>();
			std::vector < std::pair < std::size_t, std::string > > items = it->get<2>();

			for( It2 it2 = menus.begin(), it2End = menus.end(); it2 != it2End; it2++ ) {
				menuBar->SetMenuLabel( it2->first, _getStrWX( it2->second ) );
			}
			for( It2 it2 = items.begin(), it2End = items.end(); it2 != it2End; it2++ ) {
				menuBar->SetLabel( it2->first, _getStrWX( it2->second ) );
			}

			menuBar->Refresh();
		}
	}
	{
		typedef std::vector < CustomRefreshTriggerBase::Ptr >::iterator It;
		for( It it = mCustomTriggers.begin(), itEnd = mCustomTriggers.end(); it != itEnd; it++ ) {
			(*it)->trigger();
		}
	}
	{
		typedef std::vector < std::pair < wxTopLevelWindow*, std::string > >::iterator It;
		for( It it = mWnds.begin(), itEnd = mWnds.end(); it != itEnd; it++ )
		{
			it->first->SetTitle( _getStrWX( it->second ) );
			it->first->Layout();
		}
	}
}

void LangSwitcher::addWindow( wxTopLevelWindow* wnd, const std::string& label  )
{
	Poco::ScopedLock < Poco::Mutex > lock( mMtx );

	mWnds.push_back( std::pair < wxTopLevelWindow*, std::string >( wnd, label ) );
}

void LangSwitcher::addStaticBox( wxStaticBox* sbox, const std::string& label )
{
	Poco::ScopedLock < Poco::Mutex > lock( mMtx );

	mSBoxs.push_back( std::pair < wxStaticBox*, std::string >( sbox, label ) );
}

void LangSwitcher::addStaticText( wxStaticText* stxt, const std::string& label )
{
	Poco::ScopedLock < Poco::Mutex > lock( mMtx );

	mSTxts.push_back( std::pair < wxStaticText*, std::string >( stxt, label ) );
}

void LangSwitcher::addCheckBox( wxCheckBox* check, const std::string& label )
{
	Poco::ScopedLock < Poco::Mutex > lock( mMtx );

	mChecks.push_back( std::pair < wxCheckBox*, std::string >( check, label ) );
}

void LangSwitcher::addButton( wxButton* btn, const std::string& label )
{
	Poco::ScopedLock < Poco::Mutex > lock( mMtx );

	mBtns.push_back( std::pair < wxButton*, std::string >( btn, label ) );
}

void LangSwitcher::addComboBox( wxChoice* cbox, const std::vector < std::string >& labels )
{
	Poco::ScopedLock < Poco::Mutex > lock( mMtx );

	mCboxs.push_back( std::pair < wxChoice*, std::vector < std::string > >( cbox, labels ) );
}

void LangSwitcher::addMenuBar(
		wxMenuBar* mbar,
		const std::vector < std::pair < std::size_t, std::string > > menus,
		const std::vector < std::pair < std::size_t, std::string > > items
)
{
	Poco::ScopedLock < Poco::Mutex > lock( mMtx );

	mMBars.push_back( Poco::Tuple < wxMenuBar*, std::vector < std::pair < std::size_t, std::string > >, std::vector < std::pair < std::size_t, std::string > > >( mbar, menus, items ) );
}

void LangSwitcher::addCustomRefreshTrigger( CustomRefreshTriggerBase::Ptr trigger ) {
	mCustomTriggers.push_back( trigger );
}

}
}
