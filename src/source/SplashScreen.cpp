/* Copyright by: P.J. Grochowski */

#include "gdev/guiWX/SplashScreen.h"

#include "gdev/utility/Resources.h"

#include <Poco/Exception.h>

namespace gdev {
namespace guiWx {

SplashScreen::SplashScreen(wxWindow* wnd, const std::string& lbl):
		wxSplashScreen(
			_getBitmapWX(lbl),
			wxSPLASH_CENTRE_ON_SCREEN | wxSPLASH_TIMEOUT,
			1200, // milliseconds
			NULL,
			wxID_ANY,
			wxDefaultPosition,
			wxDefaultSize,
			wxBORDER_SIMPLE | wxSTAY_ON_TOP
		),
		mWnd(wnd)
{
	if(wnd == NULL) {
		throw Poco::Exception("Window pointer passed to splash screen cannot be NULL!");
	}

	Bind(wxEVT_TIMER, &SplashScreen::onNotify, this);
	Bind(wxEVT_CLOSE_WINDOW, &SplashScreen::onClose, this);
}

SplashScreen::~SplashScreen() {
	Unbind(wxEVT_TIMER, &SplashScreen::onNotify, this);
	Unbind(wxEVT_CLOSE_WINDOW, &SplashScreen::onClose, this);
}

void SplashScreen::onClose(wxCloseEvent&) {
	if(!mWnd->IsShown()) {
		mWnd->Show();
	}
	Destroy();
}
void SplashScreen::onNotify(wxTimerEvent&) {
	mWnd->Show();

	Close(true);
}

}
}
