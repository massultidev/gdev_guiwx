/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_GUIWX_CUSTOMREFRESHTRIGGER_H_
#define SRC_INCLUDE_GDEV_GUIWX_CUSTOMREFRESHTRIGGER_H_

#include "gdev/guiWX/CustomRefreshTriggerBase.h"

namespace gdev {
namespace guiWx {

template <class C>
class CustomRefreshTrigger:
		public gdev::guiWx::CustomRefreshTriggerBase
{
	public:
		typedef void (C::*Callback)();

		CustomRefreshTrigger(
				C& object,
				Callback method
		):
			mObject( &object ),
			mMethod( method )
		{}
		virtual ~CustomRefreshTrigger() {
		}

		void trigger() {
			(mObject->*mMethod)();
		}

		C* mObject;
		Callback mMethod;
};

}
}

#endif
