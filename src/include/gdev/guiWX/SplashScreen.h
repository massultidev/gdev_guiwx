/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_GUIWX_SPLASHSCREEN_H_
#define SRC_INCLUDE_GDEV_GUIWX_SPLASHSCREEN_H_

#include <wx/splash.h>

namespace gdev {
namespace guiWx {

class SplashScreen:
		public wxSplashScreen
{
	public:
		SplashScreen(wxWindow* wnd, const std::string& lbl="splash.png");
		virtual ~SplashScreen();

		void onClose(wxCloseEvent&);
		void onNotify(wxTimerEvent&);

	private:
		wxWindow* mWnd;
};

}
}

#endif
