/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_GUIWX_LANGSWITCHER_H_
#define SRC_INCLUDE_GDEV_GUIWX_LANGSWITCHER_H_

#include "gdev/guiWX/POCOwxWidgetsFix.h"

#include "gdev/guiWX/CustomRefreshTrigger.h"
#include "gdev/utility/Strings.h"

#include <wx/toplevel.h>
#include <wx/statbox.h>
#include <wx/stattext.h>
#include <wx/checkbox.h>
#include <wx/button.h>
#include <wx/choice.h>
#include <wx/menu.h>

#include <Poco/Tuple.h>

#include <string>
#include <vector>
#include <utility>

namespace gdev {
namespace guiWx {

class LangSwitcher
{
	public:
		typedef Poco::SharedPtr < LangSwitcher > Ptr;

		virtual ~LangSwitcher();

		static LangSwitcher::Ptr getInstance();

		void setLang( const std::string& lang );

		void addWindow(
				wxTopLevelWindow* wnd, //						-> 'wxFrame' or 'wxDialog' pointer.
				const std::string& label //						-> Label for string resource.
		);
		void addStaticBox(
				wxStaticBox* sbox, //							-> 'wxStaticBox' pointer.
				const std::string& label //						-> Label for string resource.
		);
		void addStaticText(
				wxStaticText* stxt, //							-> 'wxStaticText' pointer.
				const std::string& label //						-> Label for string resource.
		);
		void addCheckBox(
				wxCheckBox* check, // 							-> 'wxCheckBox' pointer.
				const std::string& label // 					-> Label for string resource.
		);
		void addButton(
				wxButton* btn, // 								-> 'wxButton' pointer.
				const std::string& label // 					-> Label for string resource.
		);
		void addComboBox(
				wxChoice* cbox, //								-> 'wxChoice' pointer.
				const std::vector < std::string >& labels //	-> Vector of labels for string resource.
		);
		void addMenuBar(
				wxMenuBar* mbar, //														-> 'wxMenuBar' pointer.
				const std::vector < std::pair < std::size_t, std::string > > menus, //	-> Vector of menu POSITION-LABEL pairs.
				const std::vector < std::pair < std::size_t, std::string > > items //	-> Vector of menu item ID-LABEL pairs.
		);
		void addCustomRefreshTrigger(
				CustomRefreshTriggerBase::Ptr trigger // 		-> Custom refresh trigger instance.
		);

	private:
		LangSwitcher();

		static LangSwitcher::Ptr mInstance;

		Poco::Mutex mMtx;
		gdev::utility::Strings::Ptr mSPtr;

		std::vector < std::pair < wxTopLevelWindow*, std::string > > mWnds;
		std::vector < std::pair < wxStaticBox*, std::string > > mSBoxs;
		std::vector < std::pair < wxStaticText*, std::string > > mSTxts;
		std::vector < std::pair < wxCheckBox*, std::string > > mChecks;
		std::vector < std::pair < wxButton*, std::string > > mBtns;
		std::vector < std::pair < wxChoice*, std::vector < std::string > > > mCboxs;
		std::vector < Poco::Tuple < wxMenuBar*, std::vector < std::pair < std::size_t, std::string > >, std::vector < std::pair < std::size_t, std::string > > > > mMBars;
		std::vector < CustomRefreshTriggerBase::Ptr > mCustomTriggers;
};

}
}

#endif
