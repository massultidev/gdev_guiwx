/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_GUIWX_DIALOGPROGRESS_H_
#define SRC_INCLUDE_GDEV_GUIWX_DIALOGPROGRESS_H_

#include "gdev/utility/Loggable.h"
#include "gdev/utility/osguard.h"

#include <wx/progdlg.h>
#include <wx/event.h>

#include <Poco/Thread.h>
#include <Poco/Dynamic/Var.h>

#if _IS_LINUX
	#include <Poco/Activity.h>
#endif

namespace gdev {
namespace guiWx {

template <class C>
class DialogProgress:
		public wxGenericProgressDialog,
		public Poco::Runnable,
		public gdev::utility::Loggable
{
	public:
		typedef bool (C::*Callback)( DialogProgress<C>&, Poco::Dynamic::Var, std::size_t);

		DialogProgress(C& obj, Callback mth, Poco::Dynamic::Var dVar, std::size_t caseIdx);
		virtual ~DialogProgress();

		void run();

		#if _IS_LINUX
			void runActivity();
		#endif

		void sendError(const wxString& msg);
		void sendWarn(const wxString& msg);

	protected:
		void onNotify(wxThreadEvent&);

		C* mObject;
		Callback mMethod;

		Poco::Thread mThr;
		Poco::Dynamic::Var mDVar;
		std::size_t mCaseIdx;

		#if _IS_LINUX
			Poco::Activity<DialogProgress> mActivity;
		#endif

		enum ThreadCommands {
			ID_PULSE,
			ID_UPDATE,
			ID_WARNING,
			ID_ERROR,
			ID_DESTROY
		};
};

}
}

//######################################################################
// Implementation:
//######################################################################

#include "gdev/utility/utility.h"
#include "gdev/utility/Strings.h"
#include "gdev/utility/Resources.h"
#include "gdev/guiWX/LangSwitcher.h"

#include <wx/msgdlg.h>

namespace gdev {
namespace guiWx {

template <class C>
DialogProgress<C>::DialogProgress(C& obj, Callback mth, Poco::Dynamic::Var dVar, std::size_t caseIdx):
		wxGenericProgressDialog(
				wxEmptyString,
				wxEmptyString,
				100,
				NULL,
				wxPD_APP_MODAL|wxPD_AUTO_HIDE|wxPD_SMOOTH
		),
		gdev::utility::Loggable("DlgProgress"),
		mObject(&obj),
		mMethod(mth),
		mDVar(dVar),
		mCaseIdx(caseIdx)
		#if _IS_LINUX
			,mActivity(this, &DialogProgress::runActivity)
		#endif
{
	TEMPLATE_INHERITANCE_GUARD(C, wxWindow);

	const std::string TITLE = "dialog_title_progress";
	SetTitle(_getStrWX(TITLE));

	SetSizeHints(wxDefaultSize, wxDefaultSize);

	Centre(wxBOTH);

    Bind(wxEVT_THREAD, &DialogProgress::onNotify, this);

	Show();
	SetFocus();
	Pulse(_getStrWX("dialog_progress_wait"));

    mThr.start( *this );
}
template <class C>
DialogProgress<C>::~DialogProgress() {
    Unbind(wxEVT_THREAD, &DialogProgress::onNotify, this);
}

template <class C>
void DialogProgress<C>::onNotify(wxThreadEvent& event) {
	switch(event.GetId()) {
		case ID_PULSE: {
			Pulse(event.GetString());
		} return;
		case ID_UPDATE: {
			Update(event.GetInt(), event.GetString());
		} return;
		case ID_WARNING: {
			wxMessageBox(
					event.GetString(),
					_getStrWX("warning"),
					wxOK|wxICON_WARNING|wxSTAY_ON_TOP|wxCENTER
			);
		} return;
		case ID_ERROR: {
			wxMessageBox(
					event.GetString(),
					_getStrWX("error"),
					wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
			);
		} return;
		case ID_DESTROY: {
			#if _IS_LINUX
				if(mActivity.isRunning()) {
					mActivity.stop();
					mActivity.wait();
				}
			#endif
			if(mThr.isRunning()) {
				mThr.join();
			}
			Destroy();
		} return;
	}
}

template <class C>
void DialogProgress<C>::sendError(const wxString& msg) {
	wxThreadEvent* event = new wxThreadEvent();
	event->SetId(ID_ERROR);
	event->SetString(msg);
	QueueEvent(event);
}
template <class C>
void DialogProgress<C>::sendWarn(const wxString& msg) {
	wxThreadEvent* event = new wxThreadEvent();
	event->SetId(ID_WARNING);
	event->SetString(msg);
	QueueEvent(event);
}

template <class C>
void DialogProgress<C>::run() {
	const std::string errLabel = "msgbox_error_unknown";
	try {
		#if _IS_LINUX
			mActivity.start();
		#endif

		if((mObject->*mMethod)(*this, mDVar, mCaseIdx)) {
			mObject->GetEventHandler()->QueueEvent(new wxThreadEvent());
		}

		#if _IS_LINUX
			if(mActivity.isRunning()) {
				mActivity.stop();
				mActivity.wait();
			}
		#endif

		wxThreadEvent event;
		event.SetId(ID_UPDATE);
		event.SetInt(100);
		event.SetString(wxEmptyString);
		AddPendingEvent(event);

	} catch(const Poco::Exception& e) {
		log().error(e.displayText());
		sendError(_getStrWX(errLabel));
	} catch(const std::exception& e) {
		log().error(std::string(e.what()));
		sendError(_getStrWX(errLabel));
	} catch(...) {
		log().error("Application encountered unknown error!");
		sendError(_getStrWX(errLabel));
	}

	wxThreadEvent* event = new wxThreadEvent();
	event->SetId(ID_DESTROY);
	QueueEvent(event);
}

#if _IS_LINUX
	template <class C>
	void DialogProgress<C>::runActivity() {
		while(!mActivity.isStopped()) {
			Poco::Thread::sleep(20);

			wxThreadEvent event;
			event.SetId(ID_PULSE);
			AddPendingEvent(event);
		}
	}
#endif

}
}

#endif
