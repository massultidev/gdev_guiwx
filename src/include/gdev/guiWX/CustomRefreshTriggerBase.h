/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_GUIWX_CUSTOMREFRESHTRIGGERBASE_H_
#define SRC_INCLUDE_GDEV_GUIWX_CUSTOMREFRESHTRIGGERBASE_H_

#include "gdev/guiWX/POCOwxWidgetsFix.h"

#include <Poco/SharedPtr.h>

namespace gdev {
namespace guiWx {

class CustomRefreshTriggerBase
{
	public:
		typedef Poco::SharedPtr<CustomRefreshTriggerBase> Ptr;

		CustomRefreshTriggerBase() {
		}
		virtual ~CustomRefreshTriggerBase() {
		}

		virtual void trigger() = 0;
};

}
}

#endif
