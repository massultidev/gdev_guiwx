/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_GUIWX_POCOWXWIDGETSFIX_H_
#define SRC_INCLUDE_GDEV_GUIWX_POCOWXWIDGETSFIX_H_

#include <wx/frame.h>
#include <wx/panel.h>
#include <wx/dialog.h>
#include <wx/statbox.h>
#include <wx/stattext.h>
#include <wx/choice.h>
#include <wx/menu.h>
#include <wx/string.h>
#include <wx/control.h>

#endif
