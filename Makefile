# Make script for: gdev::guiWx
# @Author: P.J. Grochowski

PROJECT="gdevGuiWx"
BINARY=$(PROJECT)

MAKEPID:= $(shell echo $$PPID)

NAME_MAKEFILE=$(shell basename $(abspath $(lastword $(MAKEFILE_LIST))))

DIR_MAKEFILE=$(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))
DIR_ROOT=$(shell realpath "$(DIR_MAKEFILE)")
DIR_SRC=$(DIR_ROOT)/src
DIR_CMAKE=$(DIR_ROOT)/cmake
DIR_LIB=$(DIR_ROOT)/_.lib
DIR_OBJ=$(DIR_ROOT)/_.obj

OS_LINUX="Linux"
OS_WINDOWS="Windows"

TYPE_BUILD_RELEASE="Release"
TYPE_BUILD_DEBUG="Debug"

TYPE_LINK_STATIC="static"
TYPE_LINK_SHARED="shared"

PREFIX_ERROR="[ERROR]:"
ERROR_UNKNOWN_OS="Unknown OS: \"$$(uname -s)\""

OS=$(strip $(call def_getOS))

DEPENDS_GTK2=pthread rt gthread-2.0 X11 Xxf86vm SM gtk-x11-2.0 gdk-x11-2.0 atk-1.0 gio-2.0 pangoft2-1.0 pangocairo-1.0 gdk_pixbuf-2.0 cairo pango-1.0 fontconfig gobject-2.0 glib-2.0 freetype dl m
DEPENDS_GTK3=pthread gthread-2.0 X11 Xxf86vm SM gtk-3 gdk-3 atk-1.0 gio-2.0 pangocairo-1.0 gdk_pixbuf-2.0 cairo-gobject pango-1.0 cairo gobject-2.0 glib-2.0 dl m rt

#------------------------------------------------------------------------------#

define def_error
	(echo "[ERROR]: $(1)" >&2 ; kill -TERM $(MAKEPID))
endef

define def_getOS
	$(shell $(strip \
		case "$$(uname -s | tr '[:upper:]' '[:lower:]')" in \
			(*linux*|*unix*)                    OS="$(OS_LINUX)" ;;\
			(*windows*|*cygwin*|*msys*|*mingw*) OS="$(OS_WINDOWS)" ;;\
			(*) $(call def_error, "$(ERROR_UNKNOWN_OS)") ;;\
		esac ;\
		echo "$${OS}" \
	))
endef

define def_printTarget
	$(eval SEPARATOR := "--------------------------------------------------")
	@echo "+"$(SEPARATOR)
	@echo "| Starting target: [$@]"
	@echo "+"$(SEPARATOR)
endef

define def_build
	$(eval TYPE_BUILD := $(1))
	$(eval TYPE_LINK := $(2))

	$(eval LINK := $(call def_getLinkLibraries, $(TYPE_BUILD), $(TYPE_LINK)))
	$(eval DEFINES := $(call def_getDefines, $(TYPE_BUILD), $(TYPE_LINK)))
	$(eval FLAGS := $(call def_getFlags, $(TYPE_BUILD), $(TYPE_LINK)))

	$(eval DIR_TMP := $(DIR_OBJ)/$(TYPE_BUILD)-$(TYPE_LINK))

	@mkdir -p "$(DIR_TMP)" &&\
	cd "$(DIR_TMP)" &&\
	cmake 	-G"Unix Makefiles" \
		-DCMAKE_INSTALL_PREFIX="$(DIR_LIB)" \
		-DGDEV_BUILD_TYPE="$(TYPE_BUILD)" \
		-DGDEV_LINK_MODE="$(TYPE_LINK)" \
		-DGDEV_PROJECT="$(PROJECT)" \
		-DGDEV_BINARY="$(BINARY)" \
		-DGDEV_LIBRARIES="$(LINK)" \
		-DGDEV_DEFINES="$(DEFINES)" \
		-DGDEV_FLAGS="$(FLAGS)" \
		-DGDEV_FLAGS_D="-O0" \
		-DGDEV_FLAGS_R="-O0" \
		-DGDEV_BINARY_IS_LIBRARY="ON" \
		-DGDEV_ROOT="$(DIR_SRC)" \
	"$(DIR_CMAKE)" &&\
	make &&\
	make install
endef

define def_optLib
	$(shell $(strip echo "optimized $(strip $(1)) debug $(strip $(1))d"))
endef

define def_getLinkLibraries
	$(shell $(strip \
		TYPE_BUILD=$(strip $(1)) ;\
		TYPE_LINK=$(strip $(2)) ;\
		\
		LINK="" ;\
		case "$(OS)" in \
			($(OS_LINUX)) \
				LINK="$(DEPENDS_GTK2) $${LINK}" ;;\
			($(OS_WINDOWS)) \
				LINK="$$( \
					)iphlpapi $$( \
					)opengl32 $$( \
					)glu32 $$( \
					)rpcrt4 $$( \
					)oleaut32 $$( \
					)ole32 $$( \
					)uuid $$( \
					)winspool $$( \
					)winmm $$( \
					)shell32 $$( \
					)comctl32 $$( \
					)comdlg32 $$( \
					)advapi32 $$( \
					)wsock32 $$( \
					)gdi32 $$( \
					)$${LINK}" ;;\
		esac ;\
		LINK="$$( \
			)$(call def_optLib, gdevUtility) $$( \
			)$(call def_optLib, PocoZip) $$( \
			)$(call def_optLib, PocoJSON) $$( \
			)$(call def_optLib, PocoFoundation) $$( \
			)$(call def_optLib, wxxrc) $$( \
			)$(call def_optLib, wxstc) $$( \
			)$(call def_optLib, wxrichtext) $$( \
			)$(call def_optLib, wxribbon) $$( \
			)$(call def_optLib, wxpropgrid) $$( \
			)$(call def_optLib, wxhtml) $$( \
			)$(call def_optLib, wxaui) $$( \
			)$(call def_optLib, wxadv) $$( \
			)$(call def_optLib, wxcore) $$( \
			)$(call def_optLib, wxbase) $$( \
			)$(call def_optLib, wxtiff) $$( \
			)$(call def_optLib, wxjpeg) $$( \
			)$(call def_optLib, wxpng) $$( \
			)$(call def_optLib, wxexpat) $$( \
			)$(call def_optLib, wxscintilla) $$( \
			)$${LINK}" ;\
		echo "$${LINK}" \
	))
endef

define def_getDefines
	$(shell $(strip \
		TYPE_BUILD=$(strip $(1)) ;\
		TYPE_LINK=$(strip $(2)) ;\
		\
		DEFINES="" ;\
		case "$(OS)" in \
			($(OS_LINUX)) \
				DEFINES="-fPIC -DGDEV_LINUX -D__WXGTK__ $${DEFINES}" ;;\
			($(OS_WINDOWS)) \
				DEFINES="-DGDEV_WINDOWS -D__WXMSW__ $${DEFINES}" ;;\
		esac ;\
		case "$${TYPE_BUILD}" in \
			($(TYPE_BUILD_RELEASE)) \
				DEFINES="-DNDEBUG -DwxDEBUG_LEVEL=0 $${DEFINES}" ;;\
			($(TYPE_BUILD_DEBUG)) \
				DEFINES="-D_DEBUG -DGDEV_DEBUG -DPOCO_DEBUG -DwxDEBUG_LEVEL=1 $${DEFINES}" ;;\
		esac ;\
		case "$${TYPE_LINK}" in \
			($(TYPE_LINK_STATIC)) \
				DEFINES="-DPOCO_STATIC $${DEFINES}" ;;\
			($(TYPE_LINK_SHARED)) \
				DEFINES="-DWXUSINGDLL $${DEFINES}" ;;\
		esac ;\
		echo "$${DEFINES}" \
	))
endef

define def_getFlags
	$(shell $(strip \
		TYPE_BUILD=$(strip $(1)) ;\
		TYPE_LINK=$(strip $(2)) ;\
		\
		FLAGS="" ;\
		case "$(OS)" in \
			($(OS_LINUX)) \
				FLAGS="$${FLAGS}" ;;\
			($(OS_WINDOWS)) \
				FLAGS="-static -mthreads $${FLAGS}" ;;\
		esac ;\
		echo "$${FLAGS}" \
	))
endef

#------------------------------------------------------------------------------#

all: debug

ALIAS_REL_STATIC=release
ALIAS_REL_SHARED=
ALIAS_DEB_STATIC=
ALIAS_DEB_SHARED=debug

clean:
	$(call def_printTarget)
	@rm -rfv "$(DIR_OBJ)" "$(DIR_LIB)" &&\
	echo "Success!"

release-static $(ALIAS_REL_STATIC):
	$(call def_printTarget)
	$(call def_build, \
		$(TYPE_BUILD_RELEASE), \
		$(TYPE_LINK_STATIC), \
	)

release-shared $(ALIAS_REL_SHARED):
	$(call def_printTarget)
	$(call def_build, \
		$(TYPE_BUILD_RELEASE), \
		$(TYPE_LINK_SHARED), \
	)

debug-static $(ALIAS_DEB_STATIC):
	$(call def_printTarget)
	$(call def_build, \
		$(TYPE_BUILD_DEBUG), \
		$(TYPE_LINK_STATIC), \
	)

debug-shared $(ALIAS_DEB_SHARED):
	$(call def_printTarget)
	$(call def_build, \
		$(TYPE_BUILD_DEBUG), \
		$(TYPE_LINK_SHARED), \
	)

